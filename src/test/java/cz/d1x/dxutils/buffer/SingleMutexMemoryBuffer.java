package cz.d1x.dxutils.buffer;

import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Implementation only for comparison of implementations performance.
 */
public class SingleMutexMemoryBuffer<K, V extends Bufferable> implements Buffer<K, V> {

    // need nested ConcurrentHashMap because map.keySet must be immutable during flush
    private final Object mutex = new Object();
    private final Map<K, List<V>> data = new HashMap<>();
    private final KeyFlushStrategy<K, V> flushStrategy;

    SingleMutexMemoryBuffer(KeyFlushStrategy<K, V> flushStrategy) {
        this.flushStrategy = flushStrategy;
    }

    @Override
    public LockMode getLockMode() {
        return LockMode.LOCK_BUFFER;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void put(K key, V... values) {
        put(key, Arrays.asList(values));
    }

    @SuppressWarnings("unchecked")
    @Override
    public void put(K key, Collection<V> values) {
        synchronized (mutex) {
            List<V> valueList = data.get(key);
            if (valueList == null) {
                valueList = data.computeIfAbsent(key, k -> Collections.synchronizedList(new ArrayList<>()));
            }
            valueList.addAll(values);

            if (values.stream().anyMatch(Bufferable::invokesFlush)) {
                flush(key);
            }
        }
    }

    @Override
    public Set<K> keys() {
        synchronized (mutex) {
            return new HashSet<>(data.keySet());
        }
    }

    @Override
    public Map<K, Integer> size() {
        synchronized (mutex) {
            final Map<K, Integer> sizes = new HashMap<>();
            data.forEach((k, v) -> sizes.put(k, v.size()));
            return sizes;
        }
    }

    @Override
    public List<V> valuesOf(K key) {
        synchronized (mutex) {
            List<V> values = data.get(key);
            if (values == null) return null;
            return new ArrayList<>(values);
        }
    }

    @Override
    public boolean isClear() {
        synchronized (mutex) {
            return data.isEmpty();
        }
    }

    @Override
    public boolean flush() {
        synchronized (mutex) {
            return doFlush(new HashSet<>(data.keySet()));
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public boolean flush(K... keys) {
        synchronized (mutex) {
            return doFlush(new HashSet<>(Arrays.asList(keys)));
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public boolean remove(K... keys) {
        synchronized (mutex) {
            return doRemove(Arrays.asList(keys));
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public Map<K, List<V>> removeWithoutFlush(K... keys) {
        synchronized (mutex) {
            Map<K, List<V>> removedValues = new HashMap<>();
            Arrays.stream(keys).forEach(key -> removedValues.put(key, data.remove(key)));
            return removedValues;
        }
    }

    @Override
    public boolean clear() {
        synchronized (mutex) {
            return doRemove(data.keySet());
        }
    }

    @Override
    public Map<K, List<V>> clearWithoutFlush() {
        synchronized (mutex) {
            Map<K, List<V>> removedValues = new HashMap<>();
            new HashSet<>(data.keySet()).forEach(key -> removedValues.put(key, data.remove(key)));
            return removedValues;
        }
    }

    private boolean doFlush(Collection<K> keys) {
        AtomicBoolean allFlushesOk = new AtomicBoolean(true);
        keys.forEach(key -> {
            List<V> values = data.replace(key, new ArrayList<>());
            if (values != null && !values.isEmpty()) {
                boolean flushed = false;
                try {
                    flushed = flushStrategy.flushKey(key, values);
                    if (flushed) values.clear();
                } finally {
                    if (!flushed) {
                        allFlushesOk.set(false);
                        data.put(key, values);
                    }
                }
            }
        });
        return allFlushesOk.get();
    }

    private boolean doRemove(Collection<K> keys) {
        AtomicBoolean allRemovesOk = new AtomicBoolean(true);
        keys.forEach(key -> {
            List<V> remainingData = data.get(key);
            if (remainingData != null) {
                boolean flushed = false;
                try {
                    flushed = flushStrategy.flushKey(key, remainingData);
                } finally {
                    if (!flushed) {
                        allRemovesOk.set(false);
                        data.put(key, remainingData);
                    }
                }
            }
        });
        return allRemovesOk.get();
    }

}
