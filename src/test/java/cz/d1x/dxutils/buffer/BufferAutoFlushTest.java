package cz.d1x.dxutils.buffer;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Tests {@link BufferAutoFlush}
 */
public class BufferAutoFlushTest {

    @Test
    public void isNotFlushingUponConstruction() {
        Buffer<?, ?> buffer = mockBuffer();

        BufferAutoFlush autoFlush = new BufferAutoFlush(buffer);

        Assert.assertFalse(autoFlush.isFlushing());
    }

    @Test
    public void isFlushingUponStart() {
        Buffer<?, ?> buffer = mockBuffer();

        BufferAutoFlush autoFlush = new BufferAutoFlush(buffer);
        autoFlush.startFlushing(100, TimeUnit.MILLISECONDS);

        Assert.assertTrue(autoFlush.isFlushing());
        autoFlush.stopFlushing();
    }

    @Test
    public void isNotFlushingUponStop() {
        Buffer<?, ?> buffer = mockBuffer();

        BufferAutoFlush autoFlush = new BufferAutoFlush(buffer);
        autoFlush.startFlushing(100, TimeUnit.MILLISECONDS);
        autoFlush.stopFlushing();

        Assert.assertFalse(autoFlush.isFlushing());
    }

    @Test
    public void stopsFlushingOnExceptionByDefault() throws InterruptedException {
        Buffer<?, ?> buffer = mockBufferWithExceptionOnFlush(new RuntimeException("whatever"));

        BufferAutoFlush autoFlush = new BufferAutoFlush(buffer);
        autoFlush.startFlushing(5, TimeUnit.MILLISECONDS);

        Thread.sleep(20); // 20 should be enough, should stop right after 5ms
        Assert.assertFalse(autoFlush.isFlushing());
    }

    @Test
    public void continuesFlushingOnException() throws InterruptedException {
        Buffer<?, ?> buffer = mockBufferWithExceptionOnFlush(new RuntimeException("whatever"));

        BufferAutoFlush autoFlush = new BufferAutoFlush(buffer, false);
        autoFlush.startFlushing(5, TimeUnit.MILLISECONDS);

        Thread.sleep(20); // 20 should be enough, should stop right after 5ms
        Assert.assertTrue(autoFlush.isFlushing());
        autoFlush.stopFlushing();
    }

    @Test
    public void preservesConstantSleepDelay() throws InterruptedException {
        Buffer<?, ?> buffer = mockBufferWithSleepedFlush(25);

        BufferAutoFlush autoFlush = new BufferAutoFlush(buffer, true);
        autoFlush.startFlushing(50, TimeUnit.MILLISECONDS, true);

        Thread.sleep((25 + 50 + 25) + 20); // +20 to be sure (but under 50)
        autoFlush.stopFlushing();
        Mockito.verify(buffer, Mockito.times(2)).flush(); // without constant delay it should be 3+
    }

    @Test
    public void adaptsToFlushDelay() throws InterruptedException {
        Buffer<?, ?> buffer = mockBufferWithSleepedFlush(25);

        BufferAutoFlush autoFlush = new BufferAutoFlush(buffer, true);
        autoFlush.startFlushing(50, TimeUnit.MILLISECONDS, false);

        Thread.sleep((2 * 50) + 15); // +15 to be sure (but under 25)
        autoFlush.stopFlushing();
        Mockito.verify(buffer, Mockito.times(3)).flush(); // with constant delay it should be 2
    }


    @Test
    public void callsExceptionHandlerOnExceptionAndStopsFlushing() throws InterruptedException {
        Exception ex = new RuntimeException("whatever");
        AtomicBoolean wasCaughtCorrectly = new AtomicBoolean(false);
        Buffer<?, ?> buffer = mockBufferWithExceptionOnFlush(ex);
        AutoFlushExceptionHandler exceptionHandler = (bf, exception) -> {
            wasCaughtCorrectly.set(exception.equals(ex));
            return false;
        };

        BufferAutoFlush autoFlush = new BufferAutoFlush(buffer, exceptionHandler);
        autoFlush.startFlushing(5, TimeUnit.MILLISECONDS);

        Thread.sleep(20); // 20 should be enough, should stop right after 5ms
        Assert.assertFalse(autoFlush.isFlushing());
        Assert.assertTrue(wasCaughtCorrectly.get());
        autoFlush.stopFlushing();
    }

    @Test
    public void exceptionInExceptionHandlerStopsFlushing() throws InterruptedException {
        Exception ex = new RuntimeException("whatever");
        Buffer<?, ?> buffer = mockBufferWithExceptionOnFlush(ex);
        AutoFlushExceptionHandler exceptionHandler = (bf, exception) -> {
            throw new RuntimeException("whatever in handler");
        };

        BufferAutoFlush autoFlush = new BufferAutoFlush(buffer, exceptionHandler);
        autoFlush.startFlushing(5, TimeUnit.MILLISECONDS);

        Thread.sleep(20); // 20 should be enough, should stop right after 5ms
        Assert.assertFalse(autoFlush.isFlushing());
        autoFlush.stopFlushing();

    }

    private Buffer<?, ?> mockBuffer() {
        return Mockito.mock(Buffer.class);
    }

    private Buffer<?, ?> mockBufferWithSleepedFlush(long sleepDelay) {
        Buffer<?, ?> buffer = Mockito.mock(Buffer.class);
        Mockito.when(buffer.flush()).thenAnswer(invocationOnMock -> {
            Thread.sleep(sleepDelay);
            return false;
        });
        return buffer;

    }

    private Buffer<?, ?> mockBufferWithExceptionOnFlush(Exception ex) {
        Buffer<?, ?> buffer = Mockito.mock(Buffer.class);
        Mockito.when(buffer.flush()).thenThrow(ex);
        return buffer;
    }
}
