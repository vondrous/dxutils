package cz.d1x.dxutils.buffer;

import cz.d1x.dxutils.buffer.memory.MemoryBuffer;
import cz.d1x.dxutils.buffer.memory.MemoryBufferBuilder;

/**
 * Factory for builders of {@link Buffer} implementations.
 *
 * @author Zdenek Obst, zdenek.obst-at-gmail.com
 * @see Buffer
 * @see MemoryBuffer
 * @see MemoryBufferBuilder
 */
public class Buffers {

    /**
     * Creates a new builder for {@link MemoryBuffer} construction.
     *
     * @param flushStrategy flushing strategy for the buffer
     * @param <K>           key generic of the buffer
     * @param <V>           value generic of the buffer
     * @return builder for buffer
     * @throws IllegalArgumentException possible exception if flush strategy is null
     */
    public static <K, V extends Bufferable> MemoryBufferBuilder<K, V> memory(KeyFlushStrategy<K, V> flushStrategy) {
        return new MemoryBufferBuilder<>(flushStrategy);
    }


}
