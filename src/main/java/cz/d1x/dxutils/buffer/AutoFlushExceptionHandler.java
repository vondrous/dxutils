package cz.d1x.dxutils.buffer;

/**
 * <p>
 * Handler of any exception that may be thrown during auto-flushing of buffer.
 * Every handler returns a flag whether to continue or stop flushing.
 * </p><p>
 * Note that this class is not made generic as it is made for limited use-cases.
 * Clients likely will be forced to use instanceof or other type checking mechanic to resolve current type of exception.
 * </p>
 *
 * @author Zdenek Obst, zdenek.obst-at-gmail.com
 * @see BufferAutoFlush
 */
public interface AutoFlushExceptionHandler {

    /**
     * Handles exception and returns whether to continue flushing.
     *
     * @param buffer    underlying buffer
     * @param exception caught exception
     * @return true if auto-flushing should proceed
     */
    boolean handle(Buffer<?, ?> buffer, Exception exception);
}
