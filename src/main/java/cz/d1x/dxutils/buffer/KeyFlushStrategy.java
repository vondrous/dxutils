package cz.d1x.dxutils.buffer;

import cz.d1x.dxutils.buffer.memory.MemoryBuffer;

import java.util.List;

/**
 * Strategy that is used for flushing a given key.
 * It is a callback function that typically {@link Buffer} implementations invoke during {@link Buffer#flush()}.
 *
 * @param <K> key type generic
 * @param <V> value type generic
 * @author Zdenek Obst, zdenek.obst-at-gmail.com
 * @see Buffer
 * @see MemoryBuffer
 */
public interface KeyFlushStrategy<K, V extends Bufferable> {

    /**
     * Flushes a given key and given values according to this strategy.
     * If flush is not successful (method returns false), buffer implementations may not clear flushed values
     *
     * @param key    key that is flushed
     * @param values values that are flushed
     * @return true if flush of values was successful, otherwise false
     */
    boolean flushKey(K key, List<V> values);

}
