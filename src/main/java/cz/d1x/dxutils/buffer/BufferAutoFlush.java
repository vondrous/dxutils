package cz.d1x.dxutils.buffer;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * <p>
 * A wrapper for {@link Buffer} implementations that provides auto-flushing capabilities.
 * It allows to start a single thread that periodically invokes {@link Buffer#flush()} on the underlying buffer.
 * </p><p>
 * For exception handling, clients can use constructor with {@link AutoFlushExceptionHandler} that can handle any
 * exception that is thrown during auto-flush. Eventually if clients does not need specific handling or want to
 * stop/continue flushing no matter what exception is thrown, there is constructors with simple boolean flag instead.
 * The default behavior (if you only pass a buffer) is that flushing is stopped when any exception is raised (and
 * exception is consumed by this class).
 * </p>
 *
 * @author Zdenek Obst, zdenek.obst-at-gmail.com
 * @see Buffer
 * @see AutoFlushExceptionHandler
 */
public class BufferAutoFlush {

    private final Buffer<?, ?> buffer;
    private final boolean stopFlushingOnException;
    private final AutoFlushExceptionHandler exceptionHandler;
    private final AtomicBoolean isFlushing = new AtomicBoolean(false);

    /**
     * Creates a new instance. It auto-flushes all keys of the buffer.
     * If there is an exception during flush, buffer automatically stops flushing.
     *
     * @param buffer buffer to be auto-flushed
     */
    public BufferAutoFlush(Buffer<?, ?> buffer) {
        this.buffer = buffer;
        this.stopFlushingOnException = true;
        this.exceptionHandler = null;
    }

    /**
     * Creates a new instance. It auto-flushes all keys of the buffer.
     * If there is an exception during flush, buffer stops or continues flushing according to given parameter.
     *
     * @param buffer                  buffer to be auto-flushed
     * @param stopFlushingOnException flag whether to stop flushing if exception is raised during flush
     */
    public BufferAutoFlush(Buffer<?, ?> buffer, boolean stopFlushingOnException) {
        this.buffer = buffer;
        this.stopFlushingOnException = stopFlushingOnException;
        this.exceptionHandler = null;
    }

    /**
     * Creates a new instance. It auto-flushes all keys of the buffer.
     * If there is an exception during flush, buffer stops or continues flushing according to return value of given
     * exception handler.
     *
     * @param buffer           buffer to be auto-flushed
     * @param exceptionHandler exception handler
     */
    public BufferAutoFlush(Buffer<?, ?> buffer, AutoFlushExceptionHandler exceptionHandler) {
        this.buffer = buffer;
        this.stopFlushingOnException = true;
        this.exceptionHandler = exceptionHandler;
    }

    /**
     * <p>
     * Starts a flushing thread that periodically flushes underlying buffer according the given delay.
     * There can be only a single flushing thread in the moment. If you invoke this method multiple times, it will
     * only start a single thread - only a return value will indicate that only first attempt started a thread.
     * </p><p>
     * This method preserves constant delay between flushes which means that flushing thread will always sleep the
     * same amount of time, no matter how long did flush take (e.g. if delay is 100ms, flushing starts in time 0ms,
     * flush itself takes 10m, then next flush will be invoked in time 110ms). If you don't want constant delay, use
     * {@link #startFlushing(long, TimeUnit, boolean)} instead.
     * </p>
     *
     * @param delay delay between flushes
     * @param unit  unit of the delay
     * @return true if the thread was started, false if the flushing thread is already running (thus nothing happened).
     */
    public boolean startFlushing(long delay, TimeUnit unit) {
        return startFlushing(delay, unit, true);
    }

    /**
     * <p>
     * Starts a flushing thread that periodically flushes underlying buffer according the given delay.
     * There can be only a single flushing thread in the moment. If you invoke this method multiple times, it will start
     * the thread only once - a return value will indicate that only first attempt started a thread.
     * </p><p>
     * You can specify whether there should be constant delay between flushes or if delay should adapt to the duration
     * of flush duration.
     * </p><ul>
     * <li>Constant delay is true: Flushing thread will always sleep the same amount of time, no matter how long did
     * flush take
     * (e.g. if delay is 100ms, flushing starts in time 0ms, flush itself takes 10m, then next flush will be invoked in
     * time 110ms).</li>
     * <li>Constant delay is false: Flushing thread will lower sleep time before next flush by duration of the flush
     * (e.g. if delay is 100ms, flushing starts in time 0ms, flush itself takes 10m, then next flush will be invoked in time 100ms).</li>
     * </ul>
     *
     * @param delay                       delay between flushes
     * @param unit                        unit of the delay (but milliseconds is the lowest possible unit)
     * @param constantDelayBetweenFlushes flag whether there should be constant delay between every flush invocation
     * @return true if the thread was started, false if the flushing thread is already running (thus nothing happened).
     * @throws IllegalArgumentException possible exception if given time unit is lower than millis
     */
    public boolean startFlushing(long delay, TimeUnit unit, boolean constantDelayBetweenFlushes) {
        if (unit == TimeUnit.MICROSECONDS || unit == TimeUnit.NANOSECONDS) {
            throw new IllegalArgumentException("It is allowed to use time unit of milliseconds or higher");
        }
        boolean startFlushing = isFlushing.compareAndSet(false, true);
        if (startFlushing) {
            new Thread(() -> {
                while (isFlushing.get()) {
                    long flushStart = System.currentTimeMillis();
                    try {
                        buffer.flush();
                    } catch (Exception ex) {
                        boolean continueFlushing = handleException(ex);
                        if (!continueFlushing) {
                            isFlushing.set(false);
                            break;
                        }
                    }

                    long sleepTime = unit.toMillis(delay);
                    if (!constantDelayBetweenFlushes) {
                        long flushDurationMs = System.currentTimeMillis() - flushStart;
                        sleepTime = unit.toMillis(delay) - flushDurationMs;
                    }
                    try {
                        if (sleepTime > 0) Thread.sleep(sleepTime);
                    } catch (InterruptedException e) {
                        // not interrupted from our context
                        e.printStackTrace();
                    }
                }
            }, "auto-flushed[" + buffer + "]").start();
        }
        return startFlushing;
    }

    /**
     * Stops periodical flushing.
     *
     * @return true if this method truly stopped flushing, false if no flushing was started before
     */
    public boolean stopFlushing() {
        return isFlushing.compareAndSet(true, false);
    }

    /**
     * Gets a flag whether this auto-flush is flushing or not.
     *
     * @return true if it is auto-flushing, otherwise false
     */
    public boolean isFlushing() {
        return isFlushing.get();
    }

    @SuppressWarnings("unchecked")
    private boolean handleException(Exception ex) {
        try {
            if (exceptionHandler == null) {
                return !stopFlushingOnException;
            }
            return exceptionHandler.handle(buffer, ex);
        } catch (Exception e) {
            // whoever did this is a bad bad boy (um, or girl...)
            return false; // no matter what, we stop your flushing because you must learn some manners young man (or lady)
        }
    }
}
