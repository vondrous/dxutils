package cz.d1x.dxutils.buffer;

/**
 * Interface for instances that can be stored in the {@link Buffer} implementations.
 *
 * @author Zdenek Obst, zdenek.obst-at-gmail.com
 * @see Buffer
 */
public interface Bufferable {

    /**
     * Gets a flag whether this instance should invoke direct flush during {@link Buffer#put(Object, Bufferable[])}
     * (from the calling thread).
     *
     * @return true if instance should invoke flush, otherwise false
     */
    boolean invokesFlush();
}
