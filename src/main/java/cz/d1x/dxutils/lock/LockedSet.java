package cz.d1x.dxutils.lock;

import java.util.Set;

/**
 * Predefined locked set for easier use in the code.
 * See {@link Locked} for more information.
 *
 * @author Zdenek Obst, zdenek.obst-at-gmail.com
 * @see Locked
 */
public class LockedSet<E> extends Locked<Set<E>> {

    /**
     * Creates a new locked set with given implementation.
     *
     * @param value set implementation to be used
     */
    public LockedSet(Set<E> value) {
        super(value);
    }

    /**
     * Creates a new locked set with given implementation.
     *
     * @param lock  lock to be used
     * @param value set implementation to be used
     */
    public LockedSet(Lock lock, Set<E> value) {
        super(lock, value);
    }
}
