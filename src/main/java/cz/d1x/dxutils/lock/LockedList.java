package cz.d1x.dxutils.lock;

import java.util.List;

/**
 * Predefined locked list for easier use in the code.
 * See {@link Locked} for more information.
 *
 * @author Zdenek Obst, zdenek.obst-at-gmail.com
 * @see Locked
 */
public class LockedList<E> extends Locked<List<E>> {

    /**
     * Creates a new locked list with given implementation.
     *
     * @param value list implementation to be used
     */
    public LockedList(List<E> value) {
        super(value);
    }

    /**
     * Creates a new locked list with given implementation.
     *
     * @param lock  lock to be used
     * @param value list implementation to be used
     */
    public LockedList(Lock lock, List<E> value) {
        super(lock, value);
    }
}
