package cz.d1x.dxutils.lock;

import java.util.*;

/**
 * Factory class for implementations of {@link Locked} that work with collections.
 *
 * @see Locked
 */
public class LockedCollections {

    /**
     * Creates a new locked {@link ArrayList}.
     *
     * @param <E> generic type of values
     * @return list instance
     */
    public static <E> LockedList<E> newArrayList() {
        return new LockedList<>(new ArrayList<E>());
    }

    /**
     * Creates a new locked {@link LinkedList}.
     *
     * @param <E> generic type of values
     * @return list instance
     */
    public static <E> LockedList<E> newLinkedList() {
        return new LockedList<>(new LinkedList<E>());
    }

    /**
     * Creates a new locked {@link HashSet}.
     *
     * @param <E> generic type of values
     * @return set instance
     */
    public static <E> LockedSet<E> newHashSet() {
        return new LockedSet<>(new HashSet<E>());
    }

    /**
     * Creates a new locked {@link TreeSet}.
     *
     * @param <E> generic type of values
     * @return set instance
     */
    public static <E> LockedSet<E> newTreeSet() {
        return new LockedSet<>(new TreeSet<E>());
    }

    /**
     * Creates a new locked {@link HashMap}.
     *
     * @param <K> generic type of map keys
     * @param <V> generic type of map values
     * @return map instance
     */
    public static <K, V> LockedMap<K, V> newHashMap() {
        return new LockedMap<>(new HashMap<K, V>());
    }

    /**
     * Creates a new locked {@link TreeMap}.
     *
     * @param <K> generic type of map keys
     * @param <V> generic type of map values
     * @return map instance
     */
    public static <K, V> LockedMap<K, V> newTreeMap() {
        return new LockedMap<>(new TreeMap<K, V>());
    }

    /**
     * Creates a new locked {@link ArrayList} with given lock.
     *
     * @param lock lock to be used
     * @param <E>  generic type of values
     * @return list instance
     */
    public static <E> LockedList<E> newArrayList(Lock lock) {
        return new LockedList<>(new ArrayList<E>());
    }

    /**
     * Creates a new locked {@link LinkedList} with given lock.
     *
     * @param lock lock to be used
     * @param <E>  generic type of values
     * @return list instance
     */
    public static <E> LockedList<E> newLinkedList(Lock lock) {
        return new LockedList<>(new LinkedList<E>());
    }

    /**
     * Creates a new locked {@link HashSet} with given lock.
     *
     * @param lock lock to be used
     * @param <E>  generic type of values
     * @return set instance
     */
    public static <E> LockedSet<E> newHashSet(Lock lock) {
        return new LockedSet<>(new HashSet<E>());
    }

    /**
     * Creates a new locked {@link TreeSet} with given lock.
     *
     * @param lock lock to be used
     * @param <E>  generic type of values
     * @return set instance
     */
    public static <E> LockedSet<E> newTreeSet(Lock lock) {
        return new LockedSet<>(new TreeSet<E>());
    }

    /**
     * Creates a new locked {@link HashMap} with given lock.
     *
     * @param lock lock to be used
     * @param <K>  generic type of map keys
     * @param <V>  generic type of map values
     * @return map instance
     */
    public static <K, V> LockedMap<K, V> newHashMap(Lock lock) {
        return new LockedMap<>(new HashMap<K, V>());
    }

    /**
     * Creates a new locked {@link TreeMap} with given lock.
     *
     * @param lock lock to be used
     * @param <K>  generic type of map keys
     * @param <V>  generic type of map values
     * @return map instance
     */
    public static <K, V> LockedMap<K, V> newTreeMap(Lock lock) {
        return new LockedMap<>(new TreeMap<K, V>());
    }
}
