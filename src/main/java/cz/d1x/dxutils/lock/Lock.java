package cz.d1x.dxutils.lock;

import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.function.Supplier;

/**
 * Simple lock implementation that uses Java 8 lambdas for more readable lock usage.
 * It uses {@link ReentrantReadWriteLock} from Java API.
 * If you want to use this lock exclusively for single object, consider using {@link Locked} that allows
 * to bind lock with any generic object with more elegant API.
 *
 * @author Zdenek Obst, zdenek.obst-at-gmail.com
 * @see Locked
 */
public class Lock {

    private final ReadWriteLock lockImpl;

    /**
     * Creates a new lock that won't be fair.
     */
    public Lock() {
        this(false);
    }

    /**
     * Creates a new lock.
     *
     * @param fair flag whether lock should try its best to be fair, see {@link ReentrantReadWriteLock}
     */
    public Lock(boolean fair) {
        lockImpl = new ReentrantReadWriteLock(fair);
    }

    /**
     * Performs a given action with return value in read lock.
     * Note that multiple read operations does not block themselves.
     *
     * @param action action to be performed
     * @param <R>    generic type of return value of action
     * @return return value of action
     */
    public <R> R read(Supplier<R> action) {
        java.util.concurrent.locks.Lock lock = lockImpl.readLock();
        lock.lock();
        try {
            return action.get();
        } finally {
            lock.unlock();
        }
    }

    /**
     * Performs a given action with return value in write lock.
     * Note that write lock is exclusive (so other threads attempting to read or write will be blocked until
     * given action finishes and releases the lock).
     *
     * @param action action to be performed
     * @param <R>    generic type of return value of action
     * @return return value of action
     */
    public <R> R write(Supplier<R> action) {
        java.util.concurrent.locks.Lock lock = lockImpl.writeLock();
        lock.lock();
        try {
            return action.get();
        } finally {
            lock.unlock();
        }
    }

    /**
     * Performs a given action with no return value in read lock.
     * Note that multiple read operations does not block themselves.
     *
     * @param action action to be performed
     */
    public void read(Runnable action) {
        java.util.concurrent.locks.Lock lock = lockImpl.readLock();
        lock.lock();
        try {
            action.run();
        } finally {
            lock.unlock();
        }
    }

    /**
     * Performs a given action with no return value in write lock.
     * Note that write lock is exclusive (so other threads attempting to read or write will be blocked until
     * given action finishes and releases the lock).
     *
     * @param action action to be performed
     */
    public void write(Runnable action) {
        java.util.concurrent.locks.Lock lock = lockImpl.writeLock();
        lock.lock();
        try {
            action.run();
        } finally {
            lock.unlock();
        }
    }
}
