DXUtils: Utilities
==================
Simple Java library with various utilities built purely on Java SE without any dependencies.

The utilities are mainly related to data manipulation and I/O streaming.

Maven dependency
----------------

```xml
<dependency>
   <groupId>cz.d1x</groupId>
   <artifactId>dxutils</artifactId>
   <version>0.11</version>
</dependency>
```

Features
--------

- **MemoryFileStorage** for storing data in memory if their size does not exceeds given threshold. If threshold is
reached, the data are automatically stored in backing file. Useful for applications that want to store data in memory
but may receive larger data occasionally.

- **SynchronizedDataStorage** for wrapping existing DataStorage implementations (e.g. MemoryFileStorage) to add
synchronization for all read/write operations.

- **Lock** and **Locked** Java 8+ lambda-style of read/write locks.

- **Buffer** for multi-threaded storing values per key (similarly as Map data structure) with
 possibility to drive flushing to any underlying storage. Currently single implementation **MemoryBuffer**.
  
- **BufferAutoFlush** extension of Buffer with auto-flushing capabilities once per given period of time and custom exception handling.

Examples
--------
**MemoryFileStorage**
```java
DataStorage storage = new MemoryFileStorage(1000, new File("/tmp/backing.tmp"));

// write data to storage, if over 1kB it gets automatically stored to /tmp/backing.tmp
storage.getOutputStream(); // write via OutputStream, don't forget to os.close() !!
storage.write("string data"); // UTF-8
storage.write("string data", "UTF-8"); // custom encoding
storage.write(new byte[]{0x01, 0x02, 0x03});
storage.write(new ByteArrayInputStream(new byte[]{})); // consume any InputStream
// multiple writes append existing data

// read data from storage, automatically selects source (memory or file)
storage.getInputStream(); // read via InputStream, don't forget to is.close() !!
storage.readString(); // UTF-8
storage.readString("UTF-8"); // custom encoding
storage.readBytes();

// when you are finished with storage, you should clear/close it to release resources
storage.clear(); // releases memory or deletes /tmp/backing.tmp if created

// you can use try-with-resource as DataStorage implementations are AutoCloseable
try (DataStorage autoclosedStorage = new MemoryFileStorage()) {
    autoclosedStorage.write("This will also append data to storage");
}
```

**SynchronizedDataStorage**
```java
DataStorage storage = ... // any implementation (e.g. MemoryFileStorage)
DataStorage synchronizedStorage = new SynchronizedDataStorage(storage);

synchronizedStorage.write("first"); // from any thread
synchronizedStorage.write("second"); // from any different thread

String result = synchronizedStorage.readString(); // later on
// if implementation allows appending, result will be one of "firstsecond" or "secondfirst"
```

**Locks**
```java
Lock lock = new Lock();
lock.write(() -> {
    // do your stuff, lock gets released when lambda finishes
});

List<String> list = new ArrayList<>();
Locked<List<String>> lockedList = new Locked<>(list);
lockedList.write(v -> v.add("Hello"));

// can be called from different threads as value is locked
lockedList.read(v -> System.out.println(v.size()));
lockedList.write(v -> v.add("World"));
lockedList.readWithReturn(List::size);
```

**Buffers**
```java
class MyValue implements Bufferable {
    @Override
    public boolean invokesFlush() {
       return ... // your logic if needed
    }
}

KeyFlushStrategy<Integer, MyValue> strategy = ... // strategy what to do with elements when they are flushed
Buffer<Integer, MyValue> buffer = Buffers.memory(strategy)
                                      .withFlushLockMode(FlushLockMode.LOCK_BUFFER)
                                      .withAutoRemove(false)
                                      .build();

// these can be run from multiple threads concurrently
buffer.put(0, new MyValue(...));
buffer.put(1, new MyValue(...));
buffer.put(0, new MyValue(...));
buffer.put(1, new MyValue(...));

// buffered values (if not flushed yet)
List<MyValue> valuesOf1 = buffer.valuesOf(1);

// setup auto-flushing once per 100ms
BufferAutoFlush autoFlush = new BufferAutoFlush(buffer);
autoFlush.startFlushing(100, TimeUnit.MILLISECONDS);

// also can flush specific or all keys directly
buffer.flush(1);
```